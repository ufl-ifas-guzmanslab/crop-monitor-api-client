# crop_monitor_api_client.GrafanaApi

All URIs are relative to *http://crop-monitor-admin.local*

Method | HTTP request | Description
------------- | ------------- | -------------
[**grafana_create**](GrafanaApi.md#grafana_create) | **POST** /grafana/query{format} | Grafana Simple Json Datasource Search endpoint
[**grafana_create_0**](GrafanaApi.md#grafana_create_0) | **POST** /grafana/search{format} | Grafana Simple Json Datasource Search endpoint
[**grafana_list**](GrafanaApi.md#grafana_list) | **GET** /grafana/ | 
[**grafana_query_create**](GrafanaApi.md#grafana_query_create) | **POST** /grafana/query | Grafana Simple Json Datasource Search endpoint
[**grafana_read**](GrafanaApi.md#grafana_read) | **GET** /grafana/{format} | 
[**grafana_search_create**](GrafanaApi.md#grafana_search_create) | **POST** /grafana/search | Grafana Simple Json Datasource Search endpoint


# **grafana_create**
> grafana_create(format)

Grafana Simple Json Datasource Search endpoint

Return Measurements

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.GrafanaApi(crop_monitor_api_client.ApiClient(configuration))
format = 'format_example' # str | 

try:
    # Grafana Simple Json Datasource Search endpoint
    api_instance.grafana_create(format)
except ApiException as e:
    print("Exception when calling GrafanaApi->grafana_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **grafana_create_0**
> grafana_create_0(format)

Grafana Simple Json Datasource Search endpoint

Return Metrics

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.GrafanaApi(crop_monitor_api_client.ApiClient(configuration))
format = 'format_example' # str | 

try:
    # Grafana Simple Json Datasource Search endpoint
    api_instance.grafana_create_0(format)
except ApiException as e:
    print("Exception when calling GrafanaApi->grafana_create_0: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **grafana_list**
> grafana_list()





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.GrafanaApi(crop_monitor_api_client.ApiClient(configuration))

try:
    api_instance.grafana_list()
except ApiException as e:
    print("Exception when calling GrafanaApi->grafana_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **grafana_query_create**
> grafana_query_create()

Grafana Simple Json Datasource Search endpoint

Return Measurements

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.GrafanaApi(crop_monitor_api_client.ApiClient(configuration))

try:
    # Grafana Simple Json Datasource Search endpoint
    api_instance.grafana_query_create()
except ApiException as e:
    print("Exception when calling GrafanaApi->grafana_query_create: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **grafana_read**
> grafana_read(format)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.GrafanaApi(crop_monitor_api_client.ApiClient(configuration))
format = 'format_example' # str | 

try:
    api_instance.grafana_read(format)
except ApiException as e:
    print("Exception when calling GrafanaApi->grafana_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **format** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **grafana_search_create**
> grafana_search_create()

Grafana Simple Json Datasource Search endpoint

Return Metrics

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.GrafanaApi(crop_monitor_api_client.ApiClient(configuration))

try:
    # Grafana Simple Json Datasource Search endpoint
    api_instance.grafana_search_create()
except ApiException as e:
    print("Exception when calling GrafanaApi->grafana_search_create: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

