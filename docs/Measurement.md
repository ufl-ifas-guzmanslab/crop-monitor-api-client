# Measurement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sensor_label** | **str** |  | 
**value** | **float** |  | 
**_datetime** | **datetime** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


