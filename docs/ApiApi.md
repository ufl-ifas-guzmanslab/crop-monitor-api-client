# crop_monitor_api_client.ApiApi

All URIs are relative to *http://crop-monitor-admin.local*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_clients_create**](ApiApi.md#api_clients_create) | **POST** /api/clients/ | 
[**api_clients_delete**](ApiApi.md#api_clients_delete) | **DELETE** /api/clients/{id}/ | 
[**api_clients_list**](ApiApi.md#api_clients_list) | **GET** /api/clients/ | 
[**api_clients_partial_update**](ApiApi.md#api_clients_partial_update) | **PATCH** /api/clients/{id}/ | 
[**api_clients_read**](ApiApi.md#api_clients_read) | **GET** /api/clients/{id}/ | 
[**api_clients_update**](ApiApi.md#api_clients_update) | **PUT** /api/clients/{id}/ | 
[**api_measurements_create**](ApiApi.md#api_measurements_create) | **POST** /api/measurements/ | 
[**api_measurements_delete**](ApiApi.md#api_measurements_delete) | **DELETE** /api/measurements/{id}/ | 
[**api_measurements_list**](ApiApi.md#api_measurements_list) | **GET** /api/measurements/ | 
[**api_measurements_partial_update**](ApiApi.md#api_measurements_partial_update) | **PATCH** /api/measurements/{id}/ | 
[**api_measurements_read**](ApiApi.md#api_measurements_read) | **GET** /api/measurements/{id}/ | 
[**api_measurements_update**](ApiApi.md#api_measurements_update) | **PUT** /api/measurements/{id}/ | 
[**api_sensors_create**](ApiApi.md#api_sensors_create) | **POST** /api/sensors/ | 
[**api_sensors_list**](ApiApi.md#api_sensors_list) | **GET** /api/sensors/ | 
[**api_stations_create**](ApiApi.md#api_stations_create) | **POST** /api/stations/ | 
[**api_stations_list**](ApiApi.md#api_stations_list) | **GET** /api/stations/ | 
[**api_variables_create**](ApiApi.md#api_variables_create) | **POST** /api/variables/ | 
[**api_variables_list**](ApiApi.md#api_variables_list) | **GET** /api/variables/ | 


# **api_clients_create**
> Client api_clients_create(data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.Client() # Client | 

try:
    api_response = api_instance.api_clients_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_clients_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Client**](Client.md)|  | 

### Return type

[**Client**](Client.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_clients_delete**
> api_clients_delete(id)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
id = 'id_example' # str | 

try:
    api_instance.api_clients_delete(id)
except ApiException as e:
    print("Exception when calling ApiApi->api_clients_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_clients_list**
> InlineResponse200 api_clients_list(limit=limit, offset=offset)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
limit = 56 # int | Number of results to return per page. (optional)
offset = 56 # int | The initial index from which to return the results. (optional)

try:
    api_response = api_instance.api_clients_list(limit=limit, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_clients_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Number of results to return per page. | [optional] 
 **offset** | **int**| The initial index from which to return the results. | [optional] 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_clients_partial_update**
> Client api_clients_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
id = 'id_example' # str | 
data = crop_monitor_api_client.Client() # Client | 

try:
    api_response = api_instance.api_clients_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_clients_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **data** | [**Client**](Client.md)|  | 

### Return type

[**Client**](Client.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_clients_read**
> Client api_clients_read(id)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
id = 'id_example' # str | 

try:
    api_response = api_instance.api_clients_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_clients_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**Client**](Client.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_clients_update**
> Client api_clients_update(id, data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
id = 'id_example' # str | 
data = crop_monitor_api_client.Client() # Client | 

try:
    api_response = api_instance.api_clients_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_clients_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **data** | [**Client**](Client.md)|  | 

### Return type

[**Client**](Client.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_measurements_create**
> Measurement api_measurements_create(data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.Measurement() # Measurement | 

try:
    api_response = api_instance.api_measurements_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_measurements_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Measurement**](Measurement.md)|  | 

### Return type

[**Measurement**](Measurement.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_measurements_delete**
> api_measurements_delete(id)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
id = 'id_example' # str | 

try:
    api_instance.api_measurements_delete(id)
except ApiException as e:
    print("Exception when calling ApiApi->api_measurements_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_measurements_list**
> InlineResponse2001 api_measurements_list(limit=limit, offset=offset)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
limit = 56 # int | Number of results to return per page. (optional)
offset = 56 # int | The initial index from which to return the results. (optional)

try:
    api_response = api_instance.api_measurements_list(limit=limit, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_measurements_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Number of results to return per page. | [optional] 
 **offset** | **int**| The initial index from which to return the results. | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_measurements_partial_update**
> Measurement api_measurements_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
id = 'id_example' # str | 
data = crop_monitor_api_client.Measurement() # Measurement | 

try:
    api_response = api_instance.api_measurements_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_measurements_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **data** | [**Measurement**](Measurement.md)|  | 

### Return type

[**Measurement**](Measurement.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_measurements_read**
> Measurement api_measurements_read(id)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
id = 'id_example' # str | 

try:
    api_response = api_instance.api_measurements_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_measurements_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 

### Return type

[**Measurement**](Measurement.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_measurements_update**
> Measurement api_measurements_update(id, data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
id = 'id_example' # str | 
data = crop_monitor_api_client.Measurement() # Measurement | 

try:
    api_response = api_instance.api_measurements_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_measurements_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **data** | [**Measurement**](Measurement.md)|  | 

### Return type

[**Measurement**](Measurement.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_sensors_create**
> Sensor api_sensors_create(data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.Sensor() # Sensor | 

try:
    api_response = api_instance.api_sensors_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_sensors_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Sensor**](Sensor.md)|  | 

### Return type

[**Sensor**](Sensor.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_sensors_list**
> InlineResponse2002 api_sensors_list(variable_name=variable_name, client_name=client_name, station_name=station_name, limit=limit, offset=offset)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
variable_name = 'variable_name_example' # str |  (optional)
client_name = 'client_name_example' # str |  (optional)
station_name = 'station_name_example' # str |  (optional)
limit = 56 # int | Number of results to return per page. (optional)
offset = 56 # int | The initial index from which to return the results. (optional)

try:
    api_response = api_instance.api_sensors_list(variable_name=variable_name, client_name=client_name, station_name=station_name, limit=limit, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_sensors_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **variable_name** | **str**|  | [optional] 
 **client_name** | **str**|  | [optional] 
 **station_name** | **str**|  | [optional] 
 **limit** | **int**| Number of results to return per page. | [optional] 
 **offset** | **int**| The initial index from which to return the results. | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_stations_create**
> Station api_stations_create(data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.Station() # Station | 

try:
    api_response = api_instance.api_stations_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_stations_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Station**](Station.md)|  | 

### Return type

[**Station**](Station.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_stations_list**
> InlineResponse2003 api_stations_list(name=name, client_name=client_name, limit=limit, offset=offset)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
name = 'name_example' # str |  (optional)
client_name = 'client_name_example' # str |  (optional)
limit = 56 # int | Number of results to return per page. (optional)
offset = 56 # int | The initial index from which to return the results. (optional)

try:
    api_response = api_instance.api_stations_list(name=name, client_name=client_name, limit=limit, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_stations_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  | [optional] 
 **client_name** | **str**|  | [optional] 
 **limit** | **int**| Number of results to return per page. | [optional] 
 **offset** | **int**| The initial index from which to return the results. | [optional] 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_variables_create**
> Variable api_variables_create(data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.Variable() # Variable | 

try:
    api_response = api_instance.api_variables_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_variables_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Variable**](Variable.md)|  | 

### Return type

[**Variable**](Variable.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_variables_list**
> InlineResponse2004 api_variables_list(limit=limit, offset=offset)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.ApiApi(crop_monitor_api_client.ApiClient(configuration))
limit = 56 # int | Number of results to return per page. (optional)
offset = 56 # int | The initial index from which to return the results. (optional)

try:
    api_response = api_instance.api_variables_list(limit=limit, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ApiApi->api_variables_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **int**| Number of results to return per page. | [optional] 
 **offset** | **int**| The initial index from which to return the results. | [optional] 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

