# crop_monitor_api_client.RestAuthApi

All URIs are relative to *http://crop-monitor-admin.local*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rest_auth_login_create**](RestAuthApi.md#rest_auth_login_create) | **POST** /rest-auth/login/ | 
[**rest_auth_logout_create**](RestAuthApi.md#rest_auth_logout_create) | **POST** /rest-auth/logout/ | Calls Django logout method and delete the Token object assigned to the current User object.
[**rest_auth_logout_list**](RestAuthApi.md#rest_auth_logout_list) | **GET** /rest-auth/logout/ | Calls Django logout method and delete the Token object assigned to the current User object.
[**rest_auth_password_change_create**](RestAuthApi.md#rest_auth_password_change_create) | **POST** /rest-auth/password/change/ | Calls Django Auth SetPasswordForm save method.
[**rest_auth_password_reset_confirm_create**](RestAuthApi.md#rest_auth_password_reset_confirm_create) | **POST** /rest-auth/password/reset/confirm/ | Password reset e-mail link is confirmed, therefore this resets the user&#39;s password.
[**rest_auth_password_reset_create**](RestAuthApi.md#rest_auth_password_reset_create) | **POST** /rest-auth/password/reset/ | Calls Django Auth PasswordResetForm save method.
[**rest_auth_registration_create**](RestAuthApi.md#rest_auth_registration_create) | **POST** /rest-auth/registration/ | 
[**rest_auth_registration_verify_email_create**](RestAuthApi.md#rest_auth_registration_verify_email_create) | **POST** /rest-auth/registration/verify-email/ | 
[**rest_auth_user_partial_update**](RestAuthApi.md#rest_auth_user_partial_update) | **PATCH** /rest-auth/user/ | Reads and updates UserModel fields Accepts GET, PUT, PATCH methods.
[**rest_auth_user_read**](RestAuthApi.md#rest_auth_user_read) | **GET** /rest-auth/user/ | Reads and updates UserModel fields Accepts GET, PUT, PATCH methods.
[**rest_auth_user_update**](RestAuthApi.md#rest_auth_user_update) | **PUT** /rest-auth/user/ | Reads and updates UserModel fields Accepts GET, PUT, PATCH methods.


# **rest_auth_login_create**
> Login rest_auth_login_create(data)



Check the credentials and return the REST Token if the credentials are valid and authenticated. Calls Django Auth login method to register User ID in Django session framework  Accept the following POST parameters: username, password Return the REST Framework Token Object's key.

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.Login() # Login | 

try:
    api_response = api_instance.rest_auth_login_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_login_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Login**](Login.md)|  | 

### Return type

[**Login**](Login.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rest_auth_logout_create**
> rest_auth_logout_create()

Calls Django logout method and delete the Token object assigned to the current User object.

Accepts/Returns nothing.

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))

try:
    # Calls Django logout method and delete the Token object assigned to the current User object.
    api_instance.rest_auth_logout_create()
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_logout_create: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rest_auth_logout_list**
> rest_auth_logout_list()

Calls Django logout method and delete the Token object assigned to the current User object.

Accepts/Returns nothing.

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))

try:
    # Calls Django logout method and delete the Token object assigned to the current User object.
    api_instance.rest_auth_logout_list()
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_logout_list: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rest_auth_password_change_create**
> PasswordChange rest_auth_password_change_create(data)

Calls Django Auth SetPasswordForm save method.

Accepts the following POST parameters: new_password1, new_password2 Returns the success/fail message.

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.PasswordChange() # PasswordChange | 

try:
    # Calls Django Auth SetPasswordForm save method.
    api_response = api_instance.rest_auth_password_change_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_password_change_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PasswordChange**](PasswordChange.md)|  | 

### Return type

[**PasswordChange**](PasswordChange.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rest_auth_password_reset_confirm_create**
> PasswordResetConfirm rest_auth_password_reset_confirm_create(data)

Password reset e-mail link is confirmed, therefore this resets the user's password.

Accepts the following POST parameters: token, uid,     new_password1, new_password2 Returns the success/fail message.

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.PasswordResetConfirm() # PasswordResetConfirm | 

try:
    # Password reset e-mail link is confirmed, therefore this resets the user's password.
    api_response = api_instance.rest_auth_password_reset_confirm_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_password_reset_confirm_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PasswordResetConfirm**](PasswordResetConfirm.md)|  | 

### Return type

[**PasswordResetConfirm**](PasswordResetConfirm.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rest_auth_password_reset_create**
> PasswordReset rest_auth_password_reset_create(data)

Calls Django Auth PasswordResetForm save method.

Accepts the following POST parameters: email Returns the success/fail message.

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.PasswordReset() # PasswordReset | 

try:
    # Calls Django Auth PasswordResetForm save method.
    api_response = api_instance.rest_auth_password_reset_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_password_reset_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PasswordReset**](PasswordReset.md)|  | 

### Return type

[**PasswordReset**](PasswordReset.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rest_auth_registration_create**
> CustomRegister rest_auth_registration_create(data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.CustomRegister() # CustomRegister | 

try:
    api_response = api_instance.rest_auth_registration_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_registration_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CustomRegister**](CustomRegister.md)|  | 

### Return type

[**CustomRegister**](CustomRegister.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rest_auth_registration_verify_email_create**
> VerifyEmail rest_auth_registration_verify_email_create(data)





### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.VerifyEmail() # VerifyEmail | 

try:
    api_response = api_instance.rest_auth_registration_verify_email_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_registration_verify_email_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**VerifyEmail**](VerifyEmail.md)|  | 

### Return type

[**VerifyEmail**](VerifyEmail.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rest_auth_user_partial_update**
> UserDetails rest_auth_user_partial_update(data)

Reads and updates UserModel fields Accepts GET, PUT, PATCH methods.

Default accepted fields: username, first_name, last_name Default display fields: pk, username, email, first_name, last_name Read-only fields: pk, email  Returns UserModel fields.

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.UserDetails() # UserDetails | 

try:
    # Reads and updates UserModel fields Accepts GET, PUT, PATCH methods.
    api_response = api_instance.rest_auth_user_partial_update(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_user_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UserDetails**](UserDetails.md)|  | 

### Return type

[**UserDetails**](UserDetails.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rest_auth_user_read**
> UserDetails rest_auth_user_read()

Reads and updates UserModel fields Accepts GET, PUT, PATCH methods.

Default accepted fields: username, first_name, last_name Default display fields: pk, username, email, first_name, last_name Read-only fields: pk, email  Returns UserModel fields.

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))

try:
    # Reads and updates UserModel fields Accepts GET, PUT, PATCH methods.
    api_response = api_instance.rest_auth_user_read()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_user_read: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserDetails**](UserDetails.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rest_auth_user_update**
> UserDetails rest_auth_user_update(data)

Reads and updates UserModel fields Accepts GET, PUT, PATCH methods.

Default accepted fields: username, first_name, last_name Default display fields: pk, username, email, first_name, last_name Read-only fields: pk, email  Returns UserModel fields.

### Example
```python
from __future__ import print_function
import time
import crop_monitor_api_client
from crop_monitor_api_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = crop_monitor_api_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = crop_monitor_api_client.RestAuthApi(crop_monitor_api_client.ApiClient(configuration))
data = crop_monitor_api_client.UserDetails() # UserDetails | 

try:
    # Reads and updates UserModel fields Accepts GET, PUT, PATCH methods.
    api_response = api_instance.rest_auth_user_update(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestAuthApi->rest_auth_user_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UserDetails**](UserDetails.md)|  | 

### Return type

[**UserDetails**](UserDetails.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

