# Station

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_name** | **str** |  | 
**name** | **str** |  | 
**latitude** | **str** |  | [optional] 
**longitude** | **str** |  | [optional] 
**active** | **bool** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


